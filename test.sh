test_lemipc() {
local team
local nb_team
local nb_player_team
local tmp
if [ $# -eq 1 ]; then
	team=`echo $1`
	nb_player_team=`echo "2"`
elif [ $# -eq 2 ]; then
	team=`echo $1`
	nb_player_team=`echo $2`
else
team=`echo "2"`
nb_player_team=`echo "2"`
fi
echo "test $team team."
nb_team=`echo "$team * $nb_player_team" | bc`
for i in $(seq $nb_team)
do
	tmp=`echo "$i % $team + 1" | bc`
	./lemipc $tmp &
done
}

if [ $# -eq 0 ]; then
	test_lemipc
elif [ $# -eq 1 ]; then
	test_lemipc $1
elif [ $# -eq 2 ]; then
	test_lemipc $1 $2
fi
