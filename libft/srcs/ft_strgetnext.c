/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strgetnext.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gfanton <gfanton@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/29 23:24:12 by gfanton           #+#    #+#             */
/*   Updated: 2013/12/29 23:24:13 by gfanton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strgetnext(char ***str)
{
	if (*str && **str)
	{
		if (***str)
			return (*((*str)++));
	}
	return (NULL);
}
