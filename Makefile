#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: elachere <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/09/09 14:58:19 by elachere          #+#    #+#              #
#    Updated: 2014/11/08 21:30:32 by Etienne          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = lemipc
LIB_USER = ft
LIB_DYNAMIQUE =
LIB_PAlTH =
C = \033[0;35m
NORME = ruby ~/script/ruby/rubynette/rubynette.rb
CFLAGS = -Werror -Wextra -Wall -pedantic
SRCPATH = ./srcs/
INCLUDES = \
	./includes/ \
	./libft/includes/ \

SRCS = \
	main.c \
	lemipc.c \
	map.c \
	game.c \
	sem.c \
	aff.c \

V = 0
S = 0
G = 0
N = 0
SILENCE_1 :=
SILENCE_0 :=@
SKIP_1 :=
SKIP_0 := \033[A
STAT_1 := displaystat
STAT_0 :=
NORC_1 := norme
NORC_0 :=
DEBUG_1 := -g
DEBUG_0 :=
STAT = $(STAT_$(S))
SKIP = $(SKIP_$(V))
DEBUG = $(DEBUG_$(G))
SILENCE = $(SILENCE_$(V))
NORC = $(NORC_$(N))
MYLIB = $(addprefix lib, $(LIB_USER))
ALLIB = $(addprefix -l, $(LIB_DYNAMIQUE)) $(addprefix -l, $(LIB_USER))
INCLUDE = $(addprefix -I, $(INCLUDES))
NINCLUDE = $(addsuffix *.h, $(INCLUDES))
LIB = $(addprefix -L, $(LIB_PATH)) $(addprefix -L./, $(MYLIB)) $(ALLIB)
CC = $(SILENCE)cc
RM = $(SILENCE)rm -rf
MAKE = $(SILENCE)make V=$(V) G=$(G) S=$(S) N=$(N)
SRC = $(addprefix $(SRCPATH), $(SRCS))
OBJS = $(SRC:.c=.o)
NORMESRC = $(SRC:.c)
MAKELIB = $(addprefix -C, $(MYLIB))
STATS = .makefile_stat
U = $(C)[$(NAME)]----->\033[0m

all: $(NORC) liball $(NAME) $(STAT)

$(NAME): $(OBJS)
	@echo "$(U)$(C)[COMPILE:\033[1;32m DONE$(C)]\033[0m"
	@echo "$(U)$(C)[BUILD]\033[0;32m"
	$(CC) -o $(NAME) $(OBJS) $(DEBUG) $(CFLAGS) $(INCLUDE) $(LIB)
	@echo "$(SKIP)\033[2K"
	@echo "$(SKIP)$(U)$(C)[BUILD  :\033[1;32m DONE$(C)]\033[0m"

.c.o:
	@echo "$(U)$(C)[COMPILE: \033[1;31m$<\033[A\033[0m"
	@echo "\033[0;32m"
	$(CC) -o $@ $(DEBUG) $(CFLAGS) $(INCLUDE) -c $<
	@echo "\033[1;31m [.working.]"
	@echo "$(SKIP)\033[A\033[2K$(SKIP)"

.clean:
	@echo "$(U)$(C)[CLEAN]\033[0;32m"
	$(RM) $(OBJS)
	@echo "$(SKIP)$(U)$(C)[CLEAN:\033[1;32m   DONE$(C)]\033[0m"

clean: libclean .clean

fclean: libfclean .clean
	@echo "$(U)$(C)[F-CLEAN]\033[0;32m"
	$(RM) $(NAME)
	@echo "$(SKIP)$(U)$(C)[F-CLEAN:\033[1;32m DONE$(C)]\033[0m"

liball:
	$(MAKE) all $(MAKELIB)
	@echo "$(SKIP)"

libclean:
	$(MAKE) clean $(MAKELIB)

libfclean:
	$(MAKE) fclean $(MAKELIB)

displaystat:
	@echo "$(U)$(C)[STATS]\033[0m"
	@echo "|-->[auteur: \033[0;31m`cat -e auteur |tr -d '\n'`\033[1;37m]\n"
	@echo "|-->[cfiles: `ls -l $(SRCPATH) | grep ".c$$" |wc -l| tr -d ' '`]"
	@echo "     |-->lines : `cat $(SRC) | wc -l | tr -d ' '`"
	@echo "     |-->words : `cat $(SRC) | wc -w | tr -d ' '`"
	@echo "     |-->bytes : `cat $(SRC) | wc -c | tr -d ' '`"
	@echo "\n\033[0m|--->[user symbol]"
	@echo "\033[0;31m`nm -j ./$(NAME)|grep "ft_"|cat -n`"
	@echo "\033[0m|--->[other symbol]"
	@echo "\033[0;31m`nm -j ./$(NAME)|grep -v "ft_"|cat -n`"
	@echo "\n$(U)$(C)[STATS:  \033[1;32m DONE$(C)]\033[0m"

norme:
	@echo "$(U)$(C)[NORME]\033[0;32m"
	@echo "\033[6;31mchecking srcs:\033[0;32m"
	$(SILENCE)$(NORME) $(SRC)
	@echo "\033[6;31mchecking header:\033[0;32m"
	$(SILENCE)$(NORME) $(NINCLUDE)
	@echo "$(U)$(C)[NORME:\033[1;32m DONE$(C)]\033[0m"

re: fclean all

.PHONY: clean .clean fclean
