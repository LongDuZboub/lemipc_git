#ifndef LEMIPC_H
# define LEMIPC_H

#include <sys/types.h>

# define MAP_HEIGHT 20
# define MAP_WIDTH 20
# define SHMSIZE ((MAP_HEIGHT + 1) * (MAP_WIDTH + 1))
# define PATH_SIZE 512

typedef struct	s_pos
{
	int		x;
	int		y;
}				t_pos;

typedef struct	s_lemipc
{
	int		*map;
	int		shmid;
	int		semid;
	int		first;
}				t_lemipc;

typedef struct	s_player
{
	int		team;
	t_pos	pos;
}				t_player;

int		aff();

void	v_op(int semid);
void	p_op(int semid);

void	run(t_lemipc *lemi, t_player *player);

int		full_map(int *map);
int		find_place(t_lemipc *lemi, t_player *player);
void	show_map(int *map);
void	init_map(int *map);

int		get_sem(t_lemipc *lemi, key_t key);
int		get_mem(t_lemipc *lemi, key_t key);
int		lemipc(int team);

#endif
