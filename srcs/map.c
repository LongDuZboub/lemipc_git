/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Etienne <Etienne@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 15:56:58 by Etienne           #+#    #+#             */
/*   Updated: 2014/11/09 13:05:58 by Etienne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/shm.h>
#include <stdlib.h>
#include <stdio.h>
#include "lemipc.h"
#include "libft.h"

int		full_map(int *map)
{
	int		i;
	int		j;

	i = 0;
	while (i < MAP_HEIGHT)
	{
		j = 0;
		while (j < MAP_WIDTH)
		{
			if (map[i * MAP_HEIGHT + j] == 0)
				return (1);
			++j;
		}
		++i;
	}
	return (0);
}

int		find_place(t_lemipc *lemi, t_player *player)
{
	int		r;
	int		k;

	r = rand() % MAP_HEIGHT;;
	k = rand() % MAP_WIDTH;
	p_op(lemi->semid);
	if (!full_map(lemi->map))
		return (-1);
	while (lemi->map[r * MAP_HEIGHT + k] != 0)
	{
		r = rand() % MAP_HEIGHT;
		k = rand() % MAP_WIDTH;
	}
	player->pos.x = r;
	player->pos.y = k;
	lemi->map[r * MAP_HEIGHT + k] = player->team;
	v_op(lemi->semid);
	shmdt(lemi->map);
	return (r * MAP_HEIGHT + k);
}

void	show_map(int *map)
{
	int		i;
	int		j;

	i = 0;
	while (i < MAP_HEIGHT)
	{
		j = 0;
		while (j < MAP_WIDTH)
		{
			ft_putnbr(map[i * MAP_HEIGHT + j]);
//			printf("%d", map[i * MAP_HEIGHT + j]);
			++j;
		}
		ft_putchar('\n');
//		printf("\n");
		++i;
	}
}

void	init_map(int *map)
{
	int		i;
	int		j;

	i = 0;
	while (i < MAP_HEIGHT)
	{
		j = 0;
		while (j < MAP_WIDTH)
		{
			map[i * MAP_HEIGHT + j] = 0;
			++j;
		}
		++i;
	}
}
