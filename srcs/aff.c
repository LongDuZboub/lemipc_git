/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aff.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Etienne <Etienne@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/08 16:40:24 by Etienne           #+#    #+#             */
/*   Updated: 2014/11/09 13:04:47 by Etienne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <time.h>
#include <stdio.h>
#include <sys/sem.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include "lemipc.h"


int		aff()
{
	int			i;
	char		path[SHMSIZE];
	key_t		key;
	t_lemipc	lemi;

	i = 1;
	if (!(getcwd(path, sizeof(path))))
		return (-1);
	if ((key = ftok(path, 0)) == -1)
		return (-1);
	if (get_mem(&lemi, key) != 0)
			return (-1);
//	if (get_sem(&lemi, key) != 0)
//		return (-1);
	while (i)
	{
//		p_op(lemi.semid);
		lemi.map = (int *)shmat(lemi.shmid, NULL, SHM_R | SHM_W);
		show_map(lemi.map);
		printf("\n");
		usleep(1000000);
//		v_op(lemi.semid);
		shmdt(lemi.map);
		++i;
		if (i == 30)
			exit(0);
	}
	return (0);
}
