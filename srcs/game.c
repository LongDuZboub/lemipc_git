/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Etienne <Etienne@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 22:05:38 by Etienne           #+#    #+#             */
/*   Updated: 2014/11/09 13:10:15 by Etienne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/shm.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "lemipc.h"
#include "libft.h"

void	move_down(int *map, t_player *player)
{
	if (player->pos.x + 1 < MAP_HEIGHT)
	{
		map[player->pos.x * MAP_HEIGHT + player->pos.y] = 0;
		map[(player->pos.x + 1) * MAP_HEIGHT + player->pos.y] = player->team;
		player->pos.x += 1;
	}
	else
	{
		printf("out of map\n");
		exit(0);
	}
}

void	run(t_lemipc *lemi, t_player *player)
{
	int		i;

	i = 1;
	(void)player;
//	(void)lemi;
	while (i)
	{
		lemi->map = (int *)shmat(lemi->shmid, NULL, SHM_R | SHM_W);
		p_op(lemi->semid);
//		move_down(lemi->map, player);
		usleep(1000000);
		v_op(lemi->semid);
		shmdt(lemi->map);
		i++;
		if (i == 9)
			exit(0);
	}
}
