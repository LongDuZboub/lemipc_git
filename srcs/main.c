/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Etienne <Etienne@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 15:11:22 by Etienne           #+#    #+#             */
/*   Updated: 2014/11/09 12:10:08 by Etienne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemipc.h"
#include "libft.h"

static void	usage(char *progname)
{
	ft_putstr("Usage: ./");
	ft_putstr(progname);
	ft_putendl(" <team>");
}

int			main(int ac, char **av)
{
	if (ac != 2)
		usage(av[0]);
	else
	{
		if (ft_strcmp(av[1], "printer") == 0)
			aff();
		if (lemipc(ft_atoi(av[1])) != 0)
			ft_putendl("An error occured");
	}
	return (0);
}
