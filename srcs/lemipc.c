/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemipc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Etienne <Etienne@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 15:15:25 by Etienne           #+#    #+#             */
/*   Updated: 2014/11/09 12:09:20 by Etienne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <time.h>
#include <stdio.h>
#include <sys/sem.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include "lemipc.h"

int		get_mem(t_lemipc *lemi, key_t key)
{
	if ((lemi->shmid = shmget(key, SHMSIZE, SHM_R | SHM_W)) == -1)
	{
		printf("1er appel\n");
		lemi->first = 1;
		lemi->shmid = shmget(key, SHMSIZE, IPC_CREAT | SHM_R | SHM_W);
		lemi->map = (int *)shmat(lemi->shmid, NULL, SHM_R | SHM_W);
		init_map(lemi->map);
		shmdt(lemi->map);
	}
	lemi->first = 0;
	lemi->map = (int *)shmat(lemi->shmid, NULL, SHM_R | SHM_W);
	return (0);
}

int		get_sem(t_lemipc *lemi, key_t key)
{
	if ((lemi->semid = semget(key, 1, SHM_R | SHM_W)) == -1)
	{
		if ((lemi->semid = semget(key, 1, IPC_CREAT | SHM_R | SHM_W)) == -1)
			return (-1);
		semctl(lemi->semid, 0, SETVAL, 1);
	}
	return (0);
}

int		lemipc(int team)
{
	char		path[SHMSIZE];
	key_t		key;
	t_lemipc	lemi;
	t_player	player;

	if (!(getcwd(path, sizeof(path))))
		return (-1);
	if ((key = ftok(path, 0)) == -1)
		return (-1);
	if (get_mem(&lemi, key) != 0)
		return (-1);
	if (get_sem(&lemi, key) != 0)
		return (-1);
	srand(time(NULL));
	player.team = team;
	if (find_place(&lemi, &player) < 0)
	{
		printf("Full map\n");
		exit(0);
	}
	run(&lemi, &player);
	return (0);
}
