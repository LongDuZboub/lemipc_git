/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sem.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Etienne <Etienne@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 20:06:39 by Etienne           #+#    #+#             */
/*   Updated: 2014/11/09 12:17:33 by Etienne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "errno.h"
#include "lemipc.h"
#include <sys/sem.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>

void	v_op(int semid)
{
	struct sembuf	op;

//	printf("semid (v_op) ==> %d\n", semid);
	op.sem_num = 0;
	op.sem_op = 1;
	op.sem_flg = 0;
	if (semop(semid, &op, 0) == -1)
		printf("%s\n", strerror(errno));
}

void	p_op(int semid)
{
	struct sembuf	op;

//	printf("semid (p_op) ==> %d\n", semid);
	op.sem_num = 0;
	op.sem_op = -1;
	op.sem_flg = 0;
	if (semop(semid, &op, 0) == -1)
		printf("%s\n", strerror(errno));
}
